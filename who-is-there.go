package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	// "os"
	"os/signal"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	version = "25"
	name, _ = os.Hostname()

	renderingMode = "text"

	colorReset  = "\033[0m"
	colorRed    = "\033[31m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"
	colorBlue   = "\033[34m"

	http_requests_counter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "wit",
			Name:      "http_requests_counter",
			Help:      "Total http requests counter",
		})

	time_elapsed_since_startup = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "wit",
			Name:      "time_elapsed_since_startup",
			Help:      "Time elapsed since process startup",
		})
)

func wrapper(h http.Handler) http.Handler {
	// Inspired by the article:
	// The http.Handler wrapper technique in #golang
	// https://medium.com/@matryer/the-http-handler-wrapper-technique-in-golang-updated-bc7fbcffa702

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		ip, _, _ := net.SplitHostPort(r.RemoteAddr)
		fmt.Fprintf(os.Stdout, "%s - wit v%s - %s - request on %s received from: %s\n", now.Format("2006-01-02 15:04:05"), version, name, r.URL.Path, ip)

		renderingMode = "text"
		userAgent := r.UserAgent()
		curlRegexp := regexp.MustCompile(`.*curl.*`)
		if len(curlRegexp.FindStringSubmatch(userAgent)) == 0 {
			renderingMode = "html"
		}
		h.ServeHTTP(w, r) // call original

		http_requests_counter.Inc()
	})
}

func htmlHeader(w http.ResponseWriter, r *http.Request) {
	color := os.Getenv("COLOR")
	fmt.Fprintf(w, "<!DOCTYPE html><html>")
	fmt.Fprintf(w, "<head>")
	fmt.Fprintf(w, "<title>who-is-there</title>")
	fmt.Fprintf(w, "<style>body {background-color: %s; font-size: 1em; font-family: \"Lucida Console\", Courier, monospace; margin-top: 50px; margin-left: 50px; margin-right: 280px} nav {position: fixed; right: 1em; top: 1em; width: 260px;} nav.ul {list-style-type: none;} h1 {font-size: 1.6em;} p {color: red;} </style>\n", color)
	fmt.Fprintf(w, "</head>")
	fmt.Fprintf(w, "<body>\n")
}

func htmlFooter(w http.ResponseWriter, r *http.Request) {
	userAgent := r.UserAgent()
	curlRegexp := regexp.MustCompile(`.*curl.*`)
	if len(curlRegexp.FindStringSubmatch(userAgent)) == 0 {
		fmt.Fprintf(w, "</body></html>\n")
	}
}

func htmlNav(w http.ResponseWriter) {
	fmt.Fprintf(w, "<nav>")
	fmt.Fprintf(w, "<img src=\"who-is-there.png\" alt=\"Who Is There logo\">")
	fmt.Fprintf(w, "<ul>")
	fmt.Fprintf(w, "<li><a href=\"home\">Home</a></li>")
	fmt.Fprintf(w, "<li><a href=\"env\">Environment variables</a></li>")
	fmt.Fprintf(w, "<li><a href=\"config\">Configuration</a></li>")
	// fmt.Fprintf(w, "<li><a href=\"/health\">Health check</a></li>")
	// fmt.Fprintf(w, "<li><a href=\"/metrics\">Prometheus metrics</a></li>")
	fmt.Fprintf(w, "</ul></nav>")
}

func handleRequestHome(w http.ResponseWriter, r *http.Request) {
	if renderingMode == "html" {
		w.WriteHeader(http.StatusOK)
		htmlHeader(w, r)
		fmt.Fprintf(w, "<h1>wit v%s - Hello from <strong>%s</strong></h1>\n", version, name)
		htmlNav(w)
		htmlFooter(w, r)
	} else {
		fmt.Fprintf(w, "wit v%s - Hello from %s\n", version, name)
	}
}

func handleRequestEnv(w http.ResponseWriter, r *http.Request) {
	if renderingMode == "html" {
		w.WriteHeader(http.StatusOK)
		htmlHeader(w, r)
		fmt.Fprintf(w, "<h1>Environment variables in %s</h1>\n", name)
		fmt.Fprintf(w, "<ul style='margin:0; padding:0; list-style-position: inside'>")
		for _, e := range os.Environ() {
			pair := strings.SplitN(e, "=", 2)
			fmt.Fprintf(w, "<li>%s=%s</li>\n", pair[0], pair[1])
		}
		fmt.Fprintf(w, "</ul>")
		htmlNav(w)
		htmlFooter(w, r)
	} else {
		fmt.Fprintf(w, "%sEnvironment variables in %s:%s\n", string(colorYellow), name, string(colorReset))
		for _, e := range os.Environ() {
			pair := strings.SplitN(e, "=", 2)
			fmt.Fprintf(w, "  %s=%s\n", pair[0], pair[1])
		}
	}
}

func handleRequestConfig(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadFile("config")

	if err != nil {
		log.Fatal(err)
	}

	read_lines := strings.Split(string(data), "\n")

	if renderingMode == "html" {
		w.WriteHeader(http.StatusOK)
		htmlHeader(w, r)
		fmt.Fprintf(w, "<h1>Configuration in %s</h1>\n", name)
		fmt.Fprintf(w, "<pre>\n")
		for _, line := range read_lines {
			fmt.Fprintf(w, "%s\n", line)
		}
		fmt.Fprintf(w, "</pre>\n")
		htmlNav(w)
		htmlFooter(w, r)
	} else {
		fmt.Fprintf(w, "%sConfiguration in %s:%s\n", string(colorYellow), name, string(colorReset))
		for _, line := range read_lines {
			fmt.Fprintf(w, "%s\n", line)
		}
	}
}

func handleRequestHealth(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "wit v%s - %s - I'm fine!\n", version, name)
}

func handleRequestBody(w http.ResponseWriter, r *http.Request) {
	var bodyBytes []byte
	var err error

	if r.Body != nil {
		bodyBytes, err = ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("Body reading error: %v", err)
			return
		}
		defer r.Body.Close()
	}

	fmt.Printf("Headers: %+v\n", r.Header)

	if len(bodyBytes) > 0 {
		var prettyJSON bytes.Buffer
		if err = json.Indent(&prettyJSON, bodyBytes, "", "\t"); err != nil {
			fmt.Printf("JSON parse error: %v", err)
			return
		}
		fmt.Println(prettyJSON.String())
	} else {
		fmt.Printf("Body: No Body Supplied\n")
	}
}

func handleRequestLogo(w http.ResponseWriter, r *http.Request) {
	buf, err := ioutil.ReadFile("who-is-there.png")
	if err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "image/png")

	_, err = w.Write(buf)
	if err != nil {
		log.Fatal(err)
	}
}

func multiSignalHandler(signal os.Signal) {
	now := time.Now()
	switch signal {
	case syscall.SIGTERM:
		fmt.Fprintf(os.Stdout, "%s%s - wit v%s - Exiting with SIGTERM Signal...%s\n", string(colorYellow), now.Format("2006-01-02 15:04:05"), version, string(colorReset))
		os.Exit(0)
	case syscall.SIGINT:
		fmt.Fprintf(os.Stdout, "%s%s - wit v%s - Exiting with SIGKILL Signal...%s\n", string(colorRed), now.Format("2006-01-02 15:04:05"), version, string(colorReset))
		os.Exit(0)
	default:
		fmt.Println("Unhandled/unknown signal")
	}
}

func main() {
	// Trap SIGTERM signal for graceful termination
	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, syscall.SIGINT, syscall.SIGTERM)
	// exit_chan := make(chan int)
	go func() {
		for {
			s := <-signal_chan
			multiSignalHandler(s)
		}
	}()
	// code := <-exit_chan
	// os.Exit(code)

	sleepTimeInSeconds, err := strconv.Atoi(os.Getenv("SLEEP"))
	if err != nil {
		// // handle error
		// fmt.Println(err)
		// os.Exit(2)
		sleepTimeInSeconds = 0
	}

	now := time.Now()
	fmt.Fprintf(os.Stdout, "%s%s - wit v%s - Starting...%s\n", string(colorYellow), now.Format("2006-01-02 15:04:05"), version, string(colorReset))
	time.Sleep(time.Duration(sleepTimeInSeconds) * time.Second)
	now = time.Now()
	fmt.Fprintf(os.Stdout, "%s%s - wit v%s - Listening on port 8080...%s\n", string(colorGreen), now.Format("2006-01-02 15:04:05"), version, string(colorReset))

	// Handle HTTP endpoints
	httpCatchAllHandler := http.HandlerFunc(handleRequestHome)
	http.Handle("/", wrapper(httpCatchAllHandler))

	httpHomeHandler := http.HandlerFunc(handleRequestHome)
	http.Handle("/home", wrapper(httpHomeHandler))

	httpLogoHandler := http.HandlerFunc(handleRequestLogo)
	http.Handle("/who-is-there.png", wrapper(httpLogoHandler))

	httpConfigHandler := http.HandlerFunc(handleRequestConfig)
	http.Handle("/config", wrapper(httpConfigHandler))

	httpEnvHandler := http.HandlerFunc(handleRequestEnv)
	http.Handle("/env", wrapper(httpEnvHandler))

	httpHealthHandler := http.HandlerFunc(handleRequestHealth)
	http.Handle("/health", httpHealthHandler)

	httpBodyHandler := http.HandlerFunc(handleRequestBody)
	http.Handle("/body", httpBodyHandler)

	prometheus.MustRegister(http_requests_counter)
	prometheus.MustRegister(time_elapsed_since_startup)

	http.Handle("/metrics", promhttp.Handler())

	go func() {
		for {
			time_elapsed_since_startup.Inc()
			time.Sleep(time.Second) // loop every second
		}
	}()

	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}

}
