package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Example:
// https://quii.gitbook.io/learn-go-with-tests/questions-and-answers/http-handlers-revisited

func TestHandleRequestHome(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	res := httptest.NewRecorder()

	handleRequestHome(res, req)

	// Test reponse http return code
	if res.Code != http.StatusOK {
		t.Errorf("got status %d but wanted %d", res.Code, http.StatusOK)
	}

	// Test response body
	got := res.Body.String()
	want := fmt.Sprintf("wit v%s - Hello from %s\n", version, name)
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

func TestHandleRequestHealth(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/health", nil)
	res := httptest.NewRecorder()

	handleRequestHealth(res, req)

	// Test reponse http return code
	if res.Code != http.StatusOK {
		t.Errorf("got status %d but wanted %d", res.Code, http.StatusOK)
	}

	// Test response body
	got := res.Body.String()
	want := fmt.Sprintf("wit v%s - %s - I'm fine!\n", version, name)
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

func TestHandleRequestConfig(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/health", nil)
	res := httptest.NewRecorder()

	handleRequestConfig(res, req)

	// Test reponse http return code
	if res.Code != http.StatusOK {
		t.Errorf("got status %d but wanted %d", res.Code, http.StatusOK)
	}

	// Test response body
	got := res.Body.String()
	want := fmt.Sprintf("\x1b[33mConfiguration in %s:\x1b[0m\nNo configuration provided.\n", name)
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
