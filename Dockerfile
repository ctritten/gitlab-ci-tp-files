FROM golang:1.20.5-bookworm as builder
WORKDIR /tmp/go
COPY who-is-there.go ./
COPY go.mod ./
COPY go.sum ./
RUN go build -ldflags "-linkmode external -extldflags -static" -v -o who-is-there


FROM debian:bookworm-slim
COPY --from=builder /tmp/go/who-is-there /who-is-there
LABEL maintainer ctritten@sii.fr
COPY ./who-is-there.png /
COPY ./config /
USER 1000:1000
EXPOSE 8080
ENTRYPOINT ["/who-is-there"]
